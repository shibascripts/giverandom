GiveRandom
==========

GiveRandom is a plugin made to allow for a non-complicated, no nonsense way of distributing random items (or commands) to players.
This plugin's purpose was to provide a server utility that could be used by other plugins that are available to call commands. 

Permissions
===========

GiveRandom has a few permissions, these include:
* giverandom.command.user.base - Access to the base command
* giverandom.admin.command.reload - Reloads the GiveRandom config
* giverandom.admin.command.base - Gives permission to run the /giverandom give <player> command
* giverandom.admin.command.all - Gives permission to run the /giverandom give @a command  
* giverandom.command.user.listitems - Lists items you can obtain via the giverandom command

Command Usage
=============
* /giverandom reload - Reloads the GiveRandom config
* /giverandom give <player>|@a <group> - Gives a random item to the specified player. Defaults to the current player if no player is specified, and if not ran from console. Defaults to default group in config. Optionally accepts @a for all online players
* /giverandom list <page> <group> - Iterates through and shows all possible obtainable items. Defaults to the first page. Defaults to default group in config

Config
======
This plugin uses a [YAML](https://en.wikipedia.org/wiki/YAML) configuration file. 
To ensure the yaml is valid you can put it through any yaml linter, such as [this one](http://www.yamllint.com/).
This is formatted like the below:

    mode: message
    defaultGroup: rewards
    items:
        rewards:
          diamond: {displayname: diamond, type: item, id: 'minecraft:diamond', weight: 100}
          gold: {displayname: gold, type: item, id: 'minecraft:gold_ingot', quantity: 1, weight: 100}
          stone: {displayname: stone, type: item, id: 'minecraft:stone', quantity: 64, weight: 200, message: '&6{player} &ahas won &6{quantity} {displayname}'}
          emerald: {displayname: emerald, type: item, id: 'minecraft:emerald', quantity: 1, weight: 10}
          commandtest: {displayname: commandTest, type: command, command: 'give {player} minecraft:cobblestone 1', weight: 200}
        secondaryrewards:
          diamond: {displayname: diamond, type: item, id: 'minecraft:diamond', weight: 100}
          gold: {displayname: gold, type: item, id: 'minecraft:gold_ingot', quantity: 1, weight: 100, mode: message}
          stone: {displayname: stone, type: item, id: 'minecraft:stone', quantity: 64, weight: 200}
          emerald: {displayname: emerald, type: item, id: 'minecraft:emerald', quantity: 1, weight: 10}
          commandtest: {displayname: commandTest, type: command, command: 'give {player} minecraft:cobblestone 1', weight: 200, mode: broadcast}
  * mode - Either 'message' or 'broadcast'. 'broadcast' requires a plugin (such as nucleus) with the ability to do /broadcast.
  'message' sends notification of what was won directly to the player.
  * defaultGroup - The default group for commands to run against
  * items - items is a multi-item map containing groups of rewards. Each reward should consist of:
    * The reward group name, e.g. 'rewards' or 'secondary rewards'. Each of these groups should contain each item with:
        * displayname - The name you want shown once a reward is won. For items of anything more than quantity 1 an 's' is appended to this.
        * type - The type of reward, options are item or command
        * weight - The weight at which the player wins this item. See 'Weight System' section below.
        * id (Required when type=item) - The minecraft item ID to give the player
        * quantity (Required when type=item) - The quantity of the items to give the player, defaults to 1, optionally can be a range, e.g. 1-10 which will chose a random number between the range
        * command (required when type=command) - The command you want to run from the console against the player. For substituting player use {player}
        * message (optional for both item, and command types) - Optionally override the message broadcasted or that players receive. For substituting use {player} {quantity} and {displayname}
        * skipnotification: true - Optional to allow sending a reward with no notification, useful for rewards that call other reward groups
        * mode: Either 'message' or 'broadcast' - used to overide the default setting

Weight System
=============
For calculating chance of getting an item or plugin, this plugin uses a 'weight' system. For example if you have 100 items all with a 'Weight' of 1, they will all be equally likely to be drawn.

If you have one item with a weight of 1, and one with a weight of 2, the item with the weight of two is twice as likely to be drawn.