package giverandom.giverandom.lib;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class RandomCollection<ConfigurationNode> {
    private final NavigableMap<Double, ConfigurationNode> map = new TreeMap<Double, ConfigurationNode>();
    private final Random random;
    private double total = 0;

    public RandomCollection() {
        this(new Random());
    }

    public RandomCollection(Random random) {
        this.random = random;
    }

    public RandomCollection<ConfigurationNode> add(double weight, ConfigurationNode result) {
        if (weight <= 0) return this;
        total += weight;
        map.put(total, result);
        return this;
    }

    public ConfigurationNode next() {
        double value = random.nextDouble() * total;
        return map.higherEntry(value).getValue();
    }

    public Double getPercentChance(Integer weight){
        return (double) weight / total;
    }

    public NavigableMap<Double, ConfigurationNode> getItems()
    {
        return map;
    }
}