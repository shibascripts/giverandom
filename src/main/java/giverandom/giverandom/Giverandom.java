package giverandom.giverandom;

import com.google.inject.Inject;
import giverandom.giverandom.commands.GiveRandomCommand;
import giverandom.giverandom.commands.GiveRandomListCommand;
import giverandom.giverandom.commands.GiveRandomReloadCommand;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.config.ConfigDir;
import java.nio.file.Path;

import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameLoadCompleteEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.item.inventory.EnchantItemEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.command.spec.CommandSpec;
import java.io.PrintWriter;
import java.util.HashMap;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

import giverandom.giverandom.lib.RandomCollection;

@Plugin(
        id = "giverandom",
        name = "Giverandom",
        description = "A utility plugin with configuration used to give random items, economy, etc to players!",
        url = "https://gitlab.com/shibascripts/giverandom",
        authors = {
                "nichoalsrobertm"
        }
)
public class Giverandom {

    private static Giverandom instance;

    @Inject
    private Logger logger;

    private YAMLConfigurationLoader loader;

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path privateConfigDir;

    private ConfigurationNode rootNode;

    private HashMap itemSets;

    @Listener
    public void onGameLoaded(GameLoadCompleteEvent event) {
        instance = this;
        itemSets = new HashMap<String, RandomCollection>();

        registerCommands();
        loader = YAMLConfigurationLoader.builder().setPath(Paths.get(privateConfigDir + ".yml")).build();
        reload();
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("GiveRandom Successfully started!");

    }

    @Listener
    public void onServerReload(GameReloadEvent event) {
        loadConfig();
    }

    public void registerCommands() {
        CommandSpec giveRandomSpec = CommandSpec.builder()
                .description(Text.of("Give a random item, or run a random command specified in the config file."))
                .permission("giverandom.admin.command.base")
                .arguments(
                        GenericArguments.optional(GenericArguments.firstParsing(GenericArguments.literal(Text.of("all"),"@a"), GenericArguments.player(Text.of("player")))),
                        GenericArguments.optional(GenericArguments.string(Text.of("group")))
                )
                .executor(new GiveRandomCommand())
                .build();

        CommandSpec giveRandomListSpec = CommandSpec.builder()
                .description(Text.of("Lists items that can be obtained"))
                .permission("giverandom.command.user.listitems")
                .arguments(
                        GenericArguments.optional(GenericArguments.integer(Text.of("page"))),
                        GenericArguments.optional(GenericArguments.string(Text.of("group")))
                )
                .executor(new GiveRandomListCommand())
                .build();

        CommandSpec giveRandomReloadSpec = CommandSpec.builder()
                .description(Text.of("Reloads the plugin"))
                .permission("giverandom.admin.command.reload")
                .executor(new GiveRandomReloadCommand())
                .build();

        CommandSpec giveRandomCommandSpec = CommandSpec.builder()
                .permission("giverandom.command.user.base")
                .description(Text.of("Give random items or commands to players."))
                .extendedDescription(Text.of("Usage:\n" +
                                                    "Gives player a random item\n" +
                                                    "give [player] [group]\n" +
                                                    "List all the items in a group\n" +
                                                    "list [page] [group]\n" +
                                                    "Reload's the configuration," +
                                                    "reload"))
                .child(giveRandomSpec, "give", "g")
                .child(giveRandomListSpec, "list", "l")
                .child(giveRandomReloadSpec, "reload", "r")
                .build();


        Sponge.getCommandManager().register(instance, giveRandomCommandSpec, "giverandom", "gr");

    }

    public void reload() {
        loadConfig();
    }

    public void loadConfig() {

        try{
            if(!Paths.get(privateConfigDir + ".yml").toFile().exists()){
                Paths.get(privateConfigDir + ".yml").toFile().createNewFile();
                PrintWriter pw = new PrintWriter(Paths.get(privateConfigDir + ".yml").toFile());
                pw.println("mode: message # Usage: message | broadcast\n" +
                        "defaultGroup: rewards\n" +
                        "items:\n" +
                        "  rewards:\n" +
                        "    stone: {displayname: stone, type: item, id: 'minecraft:stone', quantity: 64, weight: 200, message: '&6{player} &ahas won &6{quantity} {displayname}'}\n" +
                        "    commandtest: {displayname: commandTest, type: command, command: 'give {player} minecraft:cobblestone 1', weight: 200, mode: broadcast}\n" +
                        "    diffrewardgroup: {displayname: thirdrewards, type: command, command: 'giverandom give {player} thirdrewards',skipnotification: true, weight: 200}\n" +
                        "  secondaryrewards:\n" +
                        "    diamond: {displayname: diamond, type: item, id: 'minecraft:diamond', weight: 100}\n" +
                        "    gold: {displayname: gold, type: item, id: 'minecraft:gold_ingot', quantity: 1-5, weight: 100}\n" +
                        "    emerald: {displayname: emerald, type: item, id: 'minecraft:emerald', quantity: 1, weight: 10}\n" +
                        "  thirdrewards:\n" +
                        "    specialemerald: {displayname: Special Emerald, type: item, id: 'minecraft:emerald', quantity: 1, weight: 10, mode: message}");
                pw.close();
            }
            rootNode = loader.load();
            loader.save(rootNode);
        }catch(Exception e)
        {
            logger.error("Problem creating the config");
            logger.error(String.valueOf(e));
        }

        rootNode.getNode("items").getChildrenMap().forEach((group, node) -> {
            itemSets.put(group, new RandomCollection(){{
                rootNode.getNode("items").getNode(group).getChildrenMap().forEach((id, item) -> {
                    add(item.getNode("weight").getDouble(), item.getValue());
                });
            }});
        });

    }

    public ConfigurationNode getRootNode() {
        return rootNode;
    }

    public synchronized ConfigurationNode getNode(){
        return rootNode;
    }

    public synchronized RandomCollection getItems(String groupIn){
        return (RandomCollection) itemSets.get(groupIn);
    }

    public synchronized Set getGroups() {
        return itemSets.keySet();
    }

    public synchronized Logger getLogger (){
        return logger;
    }

    public static synchronized Giverandom getInstance() {
        return instance;
    }

}
