package giverandom.giverandom.commands;
import giverandom.giverandom.Giverandom;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.transaction.InventoryTransactionResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;


public class GiveRandomCommand implements CommandExecutor{

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        Player player = null;
        String group;

        Optional<String> all = args.getOne(Text.of("all"));

        if(!src.hasPermission("giverandom.admin.command.base")){
            src.sendMessage(Text.of("You don't have permission to run this command"));
            return CommandResult.empty();
        }

        if (!args.hasAny("group")){
            group = Giverandom.getInstance().getRootNode().getNode("defaultGroup").getString();
        }else{
            group = args.<String>getOne("group").get();
            if (!Giverandom.getInstance().getGroups().contains(group))
            {
                src.sendMessage(Text.of("Group " + group + " was not found in the config."));
                return CommandResult.empty();
            }
        }

        if (all.isPresent()) {
            for(Player p : Sponge.getServer().getOnlinePlayers()){
                processReward(src, p, group);
            }
        }else{
            if (src instanceof Player) {
                if (!args.hasAny("player")){
                    player = ((Player) src).getPlayer().get();
                }else
                {
                    player = args.<Player>getOne("player").get();
                }
            }

            if (src instanceof ConsoleSource) {
                if (!args.hasAny("player")){
                    src.sendMessage(Text.of("No player was specified, please specify a player"));
                    return CommandResult.empty();
                }else
                {
                    player = args.<Player>getOne("player").get();
                }
            }
            processReward(src, player, group);
        }



        return CommandResult.success();
    }

    private CommandResult processReward(CommandSource src, Player player, String group){
        if(!src.hasPermission("giverandom.admin.command.all")){
            src.sendMessage(Text.of("You don't have permission to give all players random rewards"));
            return CommandResult.empty();
        }

        LinkedHashMap unknownReward = (LinkedHashMap) Giverandom.getInstance().getItems(group).next();
        String type = (String) unknownReward.get("type");
        String rewardMessage = (String) unknownReward.get("message");
        String mode = (String) unknownReward.get("mode");
        Boolean skipBroadcast = (Boolean) unknownReward.get("skipnotification");

        if(skipBroadcast == null){
            skipBroadcast = false;
        }

        if(rewardMessage == null){
            rewardMessage = "You won " +  unknownReward.get("displayname");
        }

        if(mode == null){
            mode = Giverandom.getInstance().getRootNode().getNode("mode").getString();
        }

        if((!mode.equals("broadcast") && !mode.equals("message"))){
            src.sendMessage(Text.of("Mode is set to improper value, use message or broadcast"));
            return CommandResult.empty();
        }

        switch(type){
            case "command":
                Sponge.getCommandManager().process(Sponge.getServer().getConsole(), convertString((String) unknownReward.get("command"), player.getName(), (String) unknownReward.get("displayname"),1));
                String commandMessage = convertString(rewardMessage,player.getName(),(String) unknownReward.get("displayname"),1);

                if(skipBroadcast)
                {
                    // Skip sending any messages
                }
                else
                {
                    if (mode.equals("message"))
                    {
                        src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&6" + player.getName() + "&a had the command " +  unknownReward.get("displayname") + "run against them."));
                        player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize(commandMessage));
                    }else if (mode.equals("broadcast")){
                        Sponge.getCommandManager().process(Sponge.getServer().getConsole(), String.valueOf("broadcast " + commandMessage));
                    }else{
                        src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&6" + player.getName() + "&a had the command " +  unknownReward.get("displayname") + "run against them."));
                        player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize(commandMessage));
                    }
                }

            case "item":
                String playerName = player.getName();
                String id = (String) unknownReward.get("id");
                String displayName = (String) unknownReward.get("displayname");
                String unparsedQuantity = null;
                String[] unknownQuantities;
                Random r = new Random();
                Integer lowerQuantity;
                Integer maxQuantity;
                Integer quantity;

                try{
                    unparsedQuantity = (String) unknownReward.get("quantity");

                }catch(Exception e)
                {
                    unparsedQuantity = Integer.toString((Integer) unknownReward.get("quantity"));
                }
                // Default quantity of 1 for any item where one isn't specified
                if (unparsedQuantity == null) {
                    unparsedQuantity = "1";
                    break;
                }

                if (unparsedQuantity.contains("-")){
                    unknownQuantities = ((String) unknownReward.get("quantity")).split("-");
                    lowerQuantity = Integer.parseInt(unknownQuantities[0]);
                    maxQuantity = Integer.parseInt(unknownQuantities[1]);
                    quantity = r.nextInt(maxQuantity - lowerQuantity) + lowerQuantity;
                }else{
                    quantity = (Integer) unknownReward.get("quantity");
                }

                if (id == null) {
                    src.sendMessage(Text.of("Item Entry in config was specified without an ID. Please update the config and specify a minecraft ID"));
                    return CommandResult.empty();
                }

                try{
                    Sponge.getRegistry().getType(ItemType.class, id).get();
                }catch(NoSuchElementException e){
                    src.sendMessage(Text.of("Minecraft ID " + id + " does not exist. Check the ID is correct in the config."));
                    return CommandResult.empty();
                }

                if (displayName == null) {
                    displayName = id;
                }

                if (quantity > 1){
                    displayName += 's';
                }

                String itemMessage = convertString(rewardMessage,player.getName(),displayName,quantity);

                final ItemStack randomItem = ItemStack.builder()
                        .itemType(Sponge.getRegistry().getType(ItemType.class, id).get())
                        .build();
                randomItem.setQuantity(quantity);

                if(player.getInventory().offer(randomItem).getType() == InventoryTransactionResult.Type.SUCCESS) {
                    if(skipBroadcast)
                    {

                    }
                    else
                    {
                        if (mode.equals("message"))
                        {
                            src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&6" + playerName + "&a was given &6" + quantity + " " + displayName));
                            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize(itemMessage));
                        }else if (mode.equals("broadcast")){
                            Sponge.getCommandManager().process(Sponge.getServer().getConsole(), String.valueOf("broadcast " + itemMessage));
                        }else{
                            src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&6" + playerName + "&a was given &6" + quantity + " " + displayName));
                            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize(itemMessage));
                        }

                    }
                }
        }

        return CommandResult.success();
    }

    private String convertString(String unprocessedString, String player, String displayName, Integer quantity){
        String processedString = unprocessedString;
        if(processedString.contains("{player}")){
            processedString = processedString.replace("{player}", player);
        }
        if(processedString.contains("{displayname}")){
            processedString = processedString.replace("{displayname}", displayName);
        }
        if(processedString.contains("{quantity}")){
            processedString = processedString.replace("{quantity}", quantity.toString());
        }
        return processedString;
    }

}
