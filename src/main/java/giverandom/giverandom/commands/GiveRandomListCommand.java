package giverandom.giverandom.commands;
import giverandom.giverandom.Giverandom;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.concurrent.atomic.AtomicReference;


public class GiveRandomListCommand implements CommandExecutor{

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        if(!src.hasPermission("giverandom.command.user.listitems")){
            src.sendMessage(Text.of("You don't have permission to run this command"));
            return CommandResult.empty();
        }

        AtomicReference<Integer> counter = new AtomicReference<>(0);
        Integer page;
        String group;

        if (!args.hasAny("group"))
        {
            group = Giverandom.getInstance().getRootNode().getNode("defaultGroup").getString();

        }else{
            group = args.<String>getOne("group").get();
        }


        if (!args.hasAny("page"))
        {
            page = 1;
        }else if(args.<Integer>getOne("page").get() <= 0){
            page = 1;
        }else{
            page = args.<Integer>getOne("page").get();
        }


        Giverandom.getInstance().getRootNode().getNode("items").getNode(group).getChildrenMap().forEach((id, item) -> {
            if(counter.get() >= (page - 1) * 8 && counter.get() <= page * 8){
                src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&6Item: " +  item.getNode("displayname").getValue() + " &6Weight: &b" + item.getNode("weight").getValue()));
            }
            counter.updateAndGet(v -> v + 1);
        });

        return CommandResult.success();
    }
}
