package giverandom.giverandom.commands;
import giverandom.giverandom.Giverandom;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;


public class GiveRandomReloadCommand implements CommandExecutor{

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        if(!src.hasPermission("giverandom.admin.command.reload")){
            src.sendMessage(Text.of("You don't have permission to run this command"));
            return CommandResult.empty();
        }
        src.sendMessage(Text.of("GiveRandom has been reloaded."));
        Giverandom.getInstance().reload();
        return CommandResult.success();
    }
}
